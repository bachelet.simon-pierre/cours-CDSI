import requests
import time

link = "https://bbb-2.uphf.fr/bigbluebutton/presentation/a56ba9e54b5649d3cf556ba4d6b887691885395e-1615907882528/a56ba9e54b5649d3cf556ba4d6b887691885395e-1615907882528/cf8f835f8364e1b28ca765b749c66b089e8daa04-1615908195111/svg/"
canDl = True
i = 1
while canDl:
    r=requests.get(link+str(i))
    if r.status_code != 404:
        print(str(i).rjust(3,"0")+".svg")
        with open("out/"+ str(i).rjust(3,"0")+".svg", mode='wb') as localfile:     
            localfile.write(r.content)
        i=i+1
    else:
        break
    time.sleep(1)
    
