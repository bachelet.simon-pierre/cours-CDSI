# Git avec les cours de CDSI
L'objectif de ce git est de centraliser toutes les informations utiles pour les cours.

Les diapositives, support de cours, TP, TD, DS, autre...

Cela aidera aux révisions des années en cours et sera aussi utile pour les futurs étudiants



## Organisation

Les fichiers sont triés avec des tags suivants :
  - BONUS   (En bonus du cours initial donné par le prof)
  - LIVRE   (Livre en bonus)
  - INFO    (Information donnée par les élèves)
  - COURS   (Le cours en diapositives ou texte...)
  - TPxx    (Les tp notés ou non)
  - EXO     (Exo non noté)
  - SOL     (Les solutions)


# Outils, Certification, MOOC
**La liste des outils kali linux :** <http://tools.kali.org/>


### Certification :
> D'après M.Ridet

  - **Cisco :** CCNA1/CCNA2
  - **Linux :** LPIC1/LPIC2
  - **Microsoft :** MSCCNA(MSCA/MSCE)
  - **CyberSecu :** CEH(v9), OSCP
  - **Réseau :** certif HP (gratuit)
  - **Google :** Web marketing/Cloud
### MOOC :
> D'après M.Ridet
 - ANSSI
 - Cyber edu
 - CNIL (RGPD)

# Contribution et pull request
Toutes les contributions et PR (pull request) sont les bienvenues :).

Afin d'ajouter des nouveaux semestres, de combler les oublis, erreurs, fautes...

Essayer de respecter un maximum l'arborescence des fichiers et les tags :).


# Remerciment
Merci aux futurs contributeurs.

Merci aux M2 et anciens élèves pour l'idée.

Merci aux M1 qui ont envoyé des messages dans #cours que j'ai pu voler.
